#
# Be sure to run `pod lib lint DEXSideMenu.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|
  spec.name = 'DEXSideMenu'
  spec.version = '0.0.1'
  spec.ios.deployment_target = '11.0'
  spec.summary = 'By far the most fantastic side menu I have seen in my entire life over Swift language. No joke.'
  spec.description = <<-DESC
  This fantastic side menu allows you to contain different view controllers with menu view controller included, you can add any view controller that you want, yes... ANY VIEW CONTROLLER!
  DESC

  spec.homepage = 'https://bitbucket.org/digital_experiences/ios_lib_sidemenu'
  spec.license = { :type => 'MIT', :file => 'LICENSE' }
  spec.author = { 'specktro' => 'miguel.gomez@ironbit.com.mx' }
  spec.source = { :git => 'https://specktrobisne@bitbucket.org/digital_experiences/ios_lib_sidemenu.git', :tag => "#{spec.version}" }

  spec.source_files = 'SideMenu/*.swift'
  spec.frameworks = 'UIKit'
end
