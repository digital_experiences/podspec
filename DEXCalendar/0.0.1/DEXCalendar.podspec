#
# Be sure to run `pod lib lint DEXSideMenu.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|
  spec.name = 'DEXCalendar'
  spec.version = '0.0.1'
  spec.ios.deployment_target = '11.0'
  spec.summary = 'Amazing calendar project by d-iOSes 😉 😎.'
  spec.description = <<-DESC
  This fantastic side menu allows you to contain different view controllers with menu view controller included, you can add any view controller that you want, yes... ANY VIEW CONTROLLER!
  DESC
  spec.homepage = 'https://mikeShep@bitbucket.org/digital_experiences/ios_lib_calendar.git'
  spec.license = { :type => 'MIT', :file => 'LICENSE' }
  spec.author = { 'mikeShep' => 'miguel.olmedo@ironbit.com.mx' }
  spec.source = { :git => 'https://mikeShep@bitbucket.org/digital_experiences/ios_lib_calendar.git', :tag => "#{spec.version}" }
  spec.swift_version = "4.1"

  spec.source_files = 'Calendar/**/*.swift', 'Calendar/*.swift'
  spec.frameworks = 'UIKit'
end
